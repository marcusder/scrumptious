def longest_run(string):
    current_count = 1
    top_count = 0
    current_start = 0
    top_start = None
    top_end = None

    for index in range(1, len(string)):
        if string[index] == string[index - 1]:
            current_count += 1
            if current_count > top_count:
                top_count = current_count
                top_start = current_start
                top_end = index
        else:
            current_count = 1
            current_start = index

    if top_start is not None and top_end is not None:
        return top_start, top_end
    else:
        return None


def longest_run(string):

  current_count = 1
  top_count = 0
  current_start = 0
  top_start = 0
  top_end = 0

  for index in range(0, len(string)):
    if (string[index] == string[index - 1]):
      current_count += 1

      if (current_count > top_count):
        top_count = current_count
        top_start = current_start
        top_end = index

    else:
      current_count = 1
      current_start = index

  return [top_start, top_end]



def longest_run(string):
    current_count = 1
    top_count = 0
    current_start = 0
    top_start = None
    top_end = None

    for index in range(0, len(string)):
        if string[index] == string[index - 1]:
            current_count += 1
            if current_count > top_count:
                top_count = current_count
                top_start = current_start
                top_end = index
        else:
            current_count = 1
            current_start = index

    if top_start != None and top_end != None:
        return (top_start, top_end)
    else:
        return None
